<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $items = [
            'id' => 1,
            'name' => 'yogen maharjan',
            'email' => 'yogenmah04@gmail.com',
            'role' => 'Admin',
            'isActive' => '1',
            'address' => Str::random(10),
            'phoneNo' => Str::random(10),
            'password' => Hash::make('11111111'),
            'email_verified_at' => now(),
        ];
        User::create($items);
    }
}
